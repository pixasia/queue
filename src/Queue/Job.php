<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 09/08/2016
 * Time: 20:57
 */

namespace Pixasia\Queue;

/**
 * Class Job
 * @package Pixasia\Queue
 *
 * @property string 		$job      The function to run
 * @property string 		$args     Json encoded array of args to run
 * @property int 				$attempts Number of times the job has ran
 * @property string 		$status   The status of the job
 * @property \DateTime	$ran_at   The time the job was ran
 */
class Job extends \Pixasia\Model
{

	/** @var string $table The table name this Model uses */
	protected static $table = 'queue';
	/** @var string $primary_key The column for the primary key */
	protected static $primary_key = 'id';

	/** @var string QUEUED */
	const QUEUED = 'QUEUED';
	/** @var string WAITING */
	const WAITING = 'WAITING';
	/** @var string SUCCESSFUL */
	const SUCCESSFUL = 'SUCCESSFUL';
	/** @var string FAILED */
	const FAILED = 'FAILED';

	/**
	 * Job constructor.
	 * 
	 * @param array $data
	 */
	public function __construct($data = [])
	{
		parent::__construct($data);
		if (isset($data->args) && is_string($data->args)) {
			$this->data->args = json_decode($data->args, true);
		}
	}

	/**
	 * Save a job
	 *
	 * @param bool $allowSetPrimaryKey
	 * @return bool
	 */
	public function save($allowSetPrimaryKey = false)
	{
		$this->data->args = json_encode($this->data->args);
		return parent::save($allowSetPrimaryKey);
	}

	/**
	 * Update a job's status
	 *
	 * @param string $status The status to update too
	 */
	public function updateStatus($status)
	{
		\Pixasia\Database::query('UPDATE queue SET status=? WHERE id=?', [
			$status,
			$this->id
		]);

		$this->status = $status;
	}

	/**
	 * Increase the number of attempts
	 */
	public function increaseAttempts()
	{
		\Pixasia\Database::query('UPDATE queue SET attempts = attempts + 1 WHERE id=?', [
			$this->id
		]);

		$this->attempts += 1;
	}

	/**
	 * Update the ran_at time
	 */
	public function logTime()
	{
		\Pixasia\Database::query('UPDATE queue SET ran_at=NOW() WHERE id=?', [
			$this->id
		]);
	}

	/**
	 * Log an error message for a job
	 *
	 * @param string $message Error reason
	 */
	public function logFail($message)
	{
		\Pixasia\Database::query('INSERT INTO queue_log (job_id,log,created_at) VALUES(?,?,NOW())', [
			$this->id,
			$message
		]);
	}
}