<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 13/05/2016
 * Time: 23:04
 */

namespace Pixasia;

/**
 * Class Model
 * @package Pixasia
 */
class Model{

	protected static $table;

	protected static $primary_key;

	protected static $columns;

	protected static $where_sql = [];

	/**
	 * @var string the character to use to quote column names in queries
	 */
	protected static $_identifier_quote_character = '`';

	public $data;

	/**
	 * Construct project seed with data object if provided
	 *
	 * @param \stdClass $data
	 */
	public function __construct( $data = null ) {
		if (is_object( $data )) {
			$this->data = $data;
		}
	}

	public function hasData(){
		return is_object($this->data);
	}

	public function __set($name, $value)
	{
		if(!$this->hasData()){
			$this->data = new \StdClass();
		}
		$this->data->$name = $value;
	}

	/**
	 * Grab the variable
	 *
	 * @param $name
	 * @return null
	 * @throws \Exception
	 */
	public function __get($name)
	{
		if(!$this->hasData()){
			throw new \Exception('Data property hasn\'t been created');
		}

		if(property_exists($this->data,$name))
			return $this->data->$name;

		return null;
	}

	/**
	 * Test the existence of the object member from the data object
	 * if it doesn't match a native object member
	 *
	 * @param string $name
	 *
	 * @return bool
	 */
	public function __isset( $name ) {
		if ( $this->hasData() && property_exists( $this->data, $name ) ) {
			return true;
		}
		return false;
	}

	/**
	 * Get all records from the table by primary key
	 *
	 * @return mixed
	 */
	public static function all(  ) {
		$st  = Database::query(
			'SELECT * FROM `' . static::$table . '`'
		);
		$results = [];

		foreach($st->fetchAll(\PDO::FETCH_OBJ) as $row){
			$results[] = new static($row);
		}

		return $results;
	}

	/**
	 * Get one record from the table by primary key
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public static function find( $id ) {
		$st  = Database::query(
			'SELECT * FROM `' . static::$table . '` WHERE `' . static::$primary_key . '` = ? LIMIT 1',
			array( $id )
		);
		$obj = $st->fetchObject();

		return new static( $obj );
	}

	/**
	 * Alias of the find() funciton
	 *
	 * @param $id int Id of row to load
	 *
	 * @return mixed
	 */
	public static function load($id){
		return static::find($id);
	}

	/**
	 * Query the database.
	 *
	 * @param $query string The SQL query to process
	 * @param array $params An array of params to be applied to the SQL
	 *
	 * @return array
	 */
	public static function query($query, $params = []){
		return Database::query($query, $params)->fetchAll();
	}

	public static function where($key,$is,$value){

		static::$where_sql[] = [
			'sql'		=>	' ? = ? ',
			'params'	=>	[$key, $value]
		];

		return new self;
	}

	public static function get(){
		echo self::$table;
	}

	/**
	 * Quote a string that is used as an identifier
	 * (table names, column names etc). This method can
	 * also deal with dot-separated identifiers eg table.column
	 *
	 * @param string $identifier
	 *
	 * @return string
	 */
	protected static function _quote_identifier( $identifier ) {
		$class = get_called_class();
		$parts = explode( '.', $identifier );

		//Run each element of $parts through class->quotePart();
		$parts = array_map( array( $class, 'quotePart' ), $parts );

		return join( '.', $parts );
	}

	/**
	 * This method performs the actual quoting of a single
	 * part of an identifier, using the identifier quote
	 * character specified in the config (or autodetected).
	 *
	 * @param string $part
	 *
	 * @return string
	 */
	protected static function quotePart( $part ) {
		if ( $part === '*' ) {
			return $part;
		}

		return static::$_identifier_quote_character . $part . static::$_identifier_quote_character;
	}

	/**
	 * Get and cache on first call the column names associated with the current table
	 *
	 * @return array of column names for the current table
	 */
	public static function getFieldnames() {
		//If columns have already been loaded, lets not bother loading again.
		if ( ! static::$columns || ! isset( static::$columns[ static::$table ] ) ) {
			//Query database using DESCRIBE function.
			$st = Database::query( 'DESCRIBE ' . static::_quote_identifier( static::$table ) );
			//Store them statically.
			static::$columns[ static::$table ] = $st->fetchAll( \PDO::FETCH_COLUMN );
		}

		return static::$columns[ static::$table ];
	}

	/**
	 * Create an SQL fragment to be used after the SET keyword in an SQL UPDATE
	 * escaping parameters as necessary. Uses current objects data member as the data source
	 * by default the primary key is not added to the SET string, but passing $ignorePrimary as false will add it
	 *
	 * @param boolean $ignorePrimary
	 *
	 * @return string
	 */
	public function setString( $ignorePrimary = true ) {;
		$fragments = array();
		foreach ( static::getFieldnames() as $field ) {
			if ( $ignorePrimary && $field == static::$primary_key ) {
				continue;
			}
			if ( property_exists( $this->data, $field ) ) {
				if ( $this->data->$field === null ) {
					// if empty set to NULL
					$fragments[] = static::_quote_identifier( $field ) . ' = NULL';
				} else {
					// Just set value normally as not empty string with NULL allowed
					$fragments[] = static::_quote_identifier( $field ) . ' = ' . Database::getInstance()->quote( $this->data->$field );
				}
			}
		}
		$sqlFragment = implode( ", ", $fragments );

		return $sqlFragment;
	}

	/**
	 * insert a row into the database table
	 *
	 * @param bool $allowSetPrimaryKey if true include primary key field in insert (ie. you want to set it yourself)
	 *
	 * @return boolean indicating success
	 */
	public function save( $allowSetPrimaryKey = false ) {
		$this->data->updated_at = date('Y-m-d H:i:s');
		$this->data->created_at = date('Y-m-d H:i:s');

		$pk = static::$primary_key;
		if ( $allowSetPrimaryKey !== true ) {
			$this->$pk = null; // ensure id is null
		}
		$query   = 'INSERT INTO ' . static::_quote_identifier( static::$table ) . ' SET ' . $this->setString( ! $allowSetPrimaryKey );
		$st      = Database::query( $query );
		$success = ( $st->rowCount() == 1 );
		if ( $success ) {
			$this->data->{static::$primary_key} = (int) Database::lastID();
		}

		return ( $success );
	}

	/**
	 * update the current record based on primary key
	 *
	 * @return boolean indicating success
	 */
	public function update() {
		$this->data->updated_at = date('Y-m-d H:i:s');

		$query   = 'UPDATE ' . static::_quote_identifier( static::$table ) . ' SET ' . $this->setString() . ' WHERE ' . static::_quote_identifier( static::$primary_key ) . ' = ? LIMIT 1';
		$st      = Database::query(
			$query,
			array(
				$this->data->{static::$primary_key}
			)
		);
		$success = ( $st->rowCount() == 1 );

		return ( $success );
	}

	/**
	 * Delete a row from the table
	 *
	 * @throws \Exception Invalid ID
	 *
	 * @return int
	 */
	public function delete(){
		if(!isset($this->id)){
			throw new \Exception('Invalid ID');
		}
		$st = Database::query(
			'DELETE FROM `' . static::$table . '` WHERE `' . static::$primary_key . '` = ? LIMIT 1',
			array( $this->id )
		);

		return $st->rowCount();
	}
}