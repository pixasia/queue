<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 07/08/2016
 * Time: 15:33
 */

namespace Pixasia;


/**
 * Class Queue
 * @package Pixasia
 */
class Queue
{

	/**
	 * Check if a function exists
	 * @param string $callable The function the user wants to invoke.
	 * @return bool
	 * @throws \Exception
	 */
	private static function checkFunction($callable)
	{
		if ($callable == '') {
			throw new \Exception('Invalid function name');
		}

		list($class, $function) = explode('::', $callable);

		if (!method_exists($class, $function)) {
			throw new \Exception('Method does not exist');
		}

		if (!is_callable($callable)) {
			throw new \Exception('Method is not callable');
		}

		return true;
	}

	/**
	 * Enqueue a job
	 *
	 * @param string $function The function to call
	 * @param array $data Array of data to pass to the function
	 *
	 * @return int The job id
	 * @throws \Exception
	 */
	public static function enqueue($function, $data)
	{

		try {
			self::checkFunction($function);
		} catch (\Exception $e) {
			throw new \Exception('Invalid function');
		}

		//Create new job
		$job = new Queue\Job;
		$job->job = $function;
		$job->args = $data;
		$job->save();

		return $job->id;
	}

	/**
	 * Run the jobs
	 * @param int $limit The maximum number of jobs to run
	 */
	public static function run($limit = 5)
	{
		$results = \Pixasia\Database::query('SELECT * FROM queue WHERE status = \'WAITING\' LIMIT ?', [
			(int)$limit
		])->fetchAll(\PDO::FETCH_OBJ);

		//Update the status of the jobs
		self::updateJobs($results, Queue\Job::QUEUED);

		foreach ($results as $result) {
			$job = Queue\Job::find($result->id);
			self::worker($job);
		}
	}

	/**
	 * @param Queue\Job $job
	 */
	private static function worker($job)
	{

		$callable = $job->job;
		$args = $job->args;

		try {
			$job->increaseAttempts();
			$job->logTime();
			call_user_func($callable, $args);
			$job->updateStatus(Queue\Job::SUCCESSFUL);
		} catch (\Exception $e) {
			$job->updateStatus(Queue\Job::FAILED);
			$job->logFail($e->getMessage());
		}

	}

	/**
	 * Update the status for a list of jobs
	 *
	 * @param array $jobs Array of job objects
	 * @param string $status Status to update to
	 */
	private static function updateJobs($jobs, $status)
	{
		//Update jobs status
		foreach ($jobs as $job) {
			\Pixasia\Database::query('UPDATE queue SET status=? WHERE id=?', [
				$status,
				$job->id
			]);
		}
	}


}