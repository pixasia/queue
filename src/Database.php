<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 09/08/2016
 * Time: 20:58
 */

namespace Pixasia;

class Database{

	/**
	 * @var $database \PDO Holds information about the database
	 */
	private static $database = null;

	public static function getInstance(){
		return static::$database;
	}

	/**
	 * Connect to the database
	 *
	 * @param $user string The username for the database
	 * @param $pass string The password
	 * @param $database string The database name
	 *
	 * @return mixed Return the database handler
	 *
	 * @throws \Exception
	 */
	public static function connect($user, $pass, $database){
		if(static::$database !== null)
			return static::$database;

		try {
			static::$database = new \PDO('mysql:host=localhost;dbname='.$database, $user, $pass);
			static::$database->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			static::$database->setAttribute( \PDO::ATTR_EMULATE_PREPARES, false );

			return static::$database;
		} catch(\PDOException $e) {
			throw new \Exception('ERROR: ' . $e->getMessage());
		}
	}

	/**
	 * Query the database
	 *
	 * @param $sql string The query to pass to the database
	 * @param array|null $params The params to check against
	 *
	 * @return \PDOStatement
	 */
	public static function query($sql,$params = null){
		$stmt = static::$database->prepare($sql);
		$stmt->execute($params);

		return $stmt;
	}

	/**
	 * Get the last inserted ID
	 *
	 * @return int
	 */
	public static function lastID() {
		return (int) static::$database->lastInsertId();
	}

}