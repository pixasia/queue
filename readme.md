Queues
======

Queueing jobs can be done by running:

`\Pixasia\Queue::enqueue($function, $args);`

**$function:** The function to enqueue.

Example: `\Pixasia\Queue::test` will run function test() in the Queue model.

**$args:** An array of arguments to run in the function. This will be passed as the first argument to the given function.

`function test($args){
 echo $args['hello'];
}`



Running Jobs
----

Jobs can be ran by doing `\Pixasia\Queue::run($limit)`;

This should be ran on a cron job and will run up to a maximum of $limit jobs to be done.

Once a job is complete, it will update the jobs status and if it fails it will log the reason.

If a job has failed, it needs to be requeued - it currently doesn't requeue jobs.




Setup
----

Due to the queuing system using the Pixasia ORM, this needs to be initiated before any queue functions are ran.

`Pixasia\Database::connect($username, $password, $database);`

The above function will connect to the local database with the given login details.

You must run the SQL file in the sql folder. This will add two new tables to your database.

You'll need to add the repo to your project. Check the example.composer.json file to see how to do this. As it's a private repo (for now), you need
to include the auth for the repo first. Change samtidball@bitbucket to your login. 

When doing composer update, you'll be asked to type your bitbucket password.

Then below, we include the package as per normal.